﻿using UnityEngine;
using System.Collections;

public class CraneController : MonoBehaviour
{
	public GameObject cellGameObject;
	public GameObject cellTopGameObject;
	public GameObject arrowGameObject;
	public GameObject arrowElementGameObject;
	public GameObject walkAreaGameObject;
	public GameObject cabinGameObject;
	public GameObject sledGameObject;
	public GameObject hookGameObject;
	public Material hookOnMaterial;
	public Material hookOffMaterial;

	public float craneRotationSpeed = 0.3f;
	public float craneAngle = 240.0f;

	public float sledPos = -6.0f;
	public float sledSpeed = 0.04f;

	private float sledLimitMax = -3.0f;
	private float sledLimitMin = -12.0f;

	private GameObject arrowInstance;
	private GameObject walkAreaInstance;
	private GameObject cabinInstance;
	private GameObject sledInstance;
	private GameObject hookInstance;

	private Pool cellPool, topCellPool, arrowCellPool;
	private float cellHeight = 1.7f;
	private float arrowCellWidth = 3.1f;
	private Transform craneTransform;
	
	public static Transform hookedBlock;
	private Transform hookHover;

	void Awake()
	{
		arrowInstance = (GameObject)Instantiate(arrowGameObject);
		walkAreaInstance = (GameObject)Instantiate(walkAreaGameObject);
		cabinInstance = (GameObject)Instantiate(cabinGameObject);
		sledInstance = (GameObject)Instantiate(sledGameObject);
		hookInstance = (GameObject)Instantiate(hookGameObject);

		RopeScript rope = transform.GetComponent<RopeScript> ();
		rope.target = hookInstance.transform;
		rope.source = sledInstance.transform.FindChild ("Cylinder");

		craneTransform = GetComponent<Transform> ();
		cellPool = new Pool (cellGameObject, 0);
		topCellPool = new Pool (cellTopGameObject, 0);
		arrowCellPool = new Pool (arrowElementGameObject, 0);

		hookInstance.transform.GetComponent<HookCollider> ().craneController = this;
	}
	
	
	// Use this for initialization
	void Start () 
	{

	}
	
	// Update is called once per frame
	void Update () 
	{	
		bool shouldRotate = false;

        float gamepadRotateDx = Input.GetAxis("CraneRotate");

        if (gamepadRotateDx != 0.0f)
        {
            craneAngle += gamepadRotateDx * craneRotationSpeed;
            shouldRotate = true;
        }

		if (Input.GetKey (KeyCode.U)) {
			craneAngle -= craneRotationSpeed;
			shouldRotate = true;
		}
		if (Input.GetKey (KeyCode.I)) {
			craneAngle += craneRotationSpeed;
			shouldRotate = true;
		}

        if (craneAngle < 0.0f)
            craneAngle = 360.0f;
        if (craneAngle > 360.0f)
            craneAngle = 0.0f;


		if (shouldRotate)
			craneTransform.localEulerAngles = new Vector3(0.0f, craneAngle, 0.0f);

		bool shouldMove = false;

        float sledMoveDx = Input.GetAxis("SledMove");
        if (sledMoveDx != 0.0f)
        {
            sledPos = Mathf.Clamp( sledPos - sledMoveDx*sledSpeed, sledLimitMin, sledLimitMax );
            shouldMove = true;
        }

		if (Input.GetKey (KeyCode.O)) {
			sledPos = Mathf.Clamp( sledPos - sledSpeed, sledLimitMin, sledLimitMax );
			shouldMove = true;
		}
		if (Input.GetKey (KeyCode.P)) {
			sledPos = Mathf.Clamp( sledPos + sledSpeed, sledLimitMin, sledLimitMax );
			shouldMove = true;
		}

		if (shouldMove)
			sledInstance.transform.localPosition = new Vector3( sledPos, sledInstance.transform.localPosition.y, sledInstance.transform.localPosition.z);
	
		if (Input.GetKey (KeyCode.J) || Input.GetAxis("Lift") == -1.0f) 
		{
			RopeScript rope = transform.GetComponent<RopeScript> ();
			rope.adjustRope(-0.01f);
		}
       
        Debug.Log(Input.GetAxis("BringDown"));

		if (Input.GetKey(KeyCode.K) || Input.GetAxis("BringDown") == -1.0f)
		{
			RopeScript rope = transform.GetComponent<RopeScript> ();
			rope.adjustRope(0.01f);
		}
		if ( (Input.GetButton("Hook") || Input.GetKey(KeyCode.H)) && null != hookHover) 
		{
			hookBlock( hookHover );
		}
		if ( (Input.GetButton("Unhook") || Input.GetKey(KeyCode.G)) && null != hookedBlock) 
		{
			unhook();
		}


	}


	public void setCells(int cellCount)
	{
		float x = 0;
		float y = 0;
		float z = 0;

		int topcellCount = Mathf.Min (4, cellCount - 1);
		GameObject go;

		y += 0.9f;
		cellPool.deactivateAll ();
		for (int i=0; i<cellCount; ++i) {
			go = cellPool.GetPooledObject ();
			go.transform.parent = craneTransform;
			go.transform.localPosition = new Vector3 (x, y, z);
			go.transform.hasChanged = true;
			go.SetActive (true);

			y += cellHeight;
		}

		float yTop = y = y - cellHeight;
		topCellPool.deactivateAll ();
		for (int i=0; i<topcellCount; ++i) 
		{
			go = topCellPool.GetPooledObject ();
			go.transform.parent = craneTransform;
			go.transform.localPosition = new Vector3 (x, y, z);
			go.transform.hasChanged = true;
			go.SetActive (true);

			y -= cellHeight;
		}

		go = arrowInstance;
		go.transform.parent = craneTransform;
		go.transform.localPosition = new Vector3 (x, yTop + 0.9f, z);
		go.transform.hasChanged = true;
		go.SetActive (true);		

		go = walkAreaInstance;
		go.transform.parent = craneTransform;
		go.transform.localPosition = new Vector3 (x, yTop - cellHeight*1.5f, z);
		go.transform.hasChanged = true;
		go.SetActive (true);		

		go = cabinInstance;
		go.transform.parent = craneTransform;
		go.transform.localPosition = new Vector3 (x, yTop - cellHeight, -1.8f);
		go.transform.hasChanged = true;
		go.SetActive (true);		


		go = sledInstance;
		go.transform.parent = arrowInstance.transform;
		go.transform.localPosition = new Vector3 (sledPos, 0.0f, 0.0f);
		go.transform.hasChanged = true;
		go.SetActive (true);		

		go = hookInstance;
		go.transform.parent = sledInstance.transform;
		go.transform.localPosition = new Vector3 (0.0f, -yTop+0.5f, 0.0f);
		go.transform.Rotate (0, 30, 0);
		go.transform.hasChanged = true;
		go.SetActive (true);		

		RopeScript rope = transform.GetComponent<RopeScript> ();
		rope.ropeLength = yTop;
		rope.BuildRope();

		// XXX you do not want to rotate the arrow instance before arrow cells are attached to it
	}

	public void setArrowCells(int cellCount)
	{
		float x = 0;
		arrowCellPool.deactivateAll ();

		for (int i=0; i<cellCount; ++i) 
		{
			GameObject go = arrowCellPool.GetPooledObject();
			go.transform.parent = arrowInstance.transform;
			go.transform.localRotation.Set(0,0,0,0);
			go.transform.localPosition = new Vector3 (x, 0, 0);
			go.transform.hasChanged = true;
			go.SetActive (true);			

			x -= arrowCellWidth;
		}

		sledLimitMin = x + sledLimitMax;

		craneTransform.localEulerAngles = new Vector3(0.0f, craneAngle, 0.0f);

	}


	public void OnTriggerEnter(Collider other) 
	{
		if (hookedBlock == null) 
		{
			if (other.gameObject.CompareTag ("HookPoint")) 
			{
				other.gameObject.GetComponent<MeshRenderer>().material = hookOnMaterial;
				hookHover = other.gameObject.transform;
			}
		}
	}

	public void OnTriggerExit(Collider other)
	{
		if (hookedBlock == null) 
		{
			if (other.gameObject.CompareTag ("HookPoint")) 
			{
				other.gameObject.GetComponent<MeshRenderer>().material = hookOffMaterial;
				hookHover = null;
			}
		}
	}


	private void hookBlock( Transform block )
	{
		if (null == hookedBlock) 
		{
			hookedBlock = block;
			hookedBlock.GetComponent<MeshRenderer>().material = hookOnMaterial;

			ConfigurableJoint joint = hookedBlock.GetComponent<ConfigurableJoint>();

			hookInstance.GetComponent<Rigidbody>().position =  hookedBlock.GetComponent<Rigidbody>().position; 
			joint.connectedBody = hookInstance.GetComponent<Rigidbody>();
					
			joint.xMotion = ConfigurableJointMotion.Locked;
			joint.yMotion = ConfigurableJointMotion.Locked;
			joint.zMotion = ConfigurableJointMotion.Locked;

		}
	}


	private void unhook()
	{
		if (hookedBlock != null) 
		{
			hookedBlock.GetComponent<MeshRenderer>().material = hookOffMaterial;

			ConfigurableJoint joint = hookedBlock.GetComponent<ConfigurableJoint>();
			joint.connectedBody = null;  

			joint.xMotion = ConfigurableJointMotion.Free;
			joint.yMotion = ConfigurableJointMotion.Free;
			joint.zMotion = ConfigurableJointMotion.Free;

			hookedBlock = null;
		}
	}
}
