﻿using UnityEngine;
using System.Collections;

public class HookCollider : MonoBehaviour 
{

	public CraneController craneController; 

	void OnTriggerEnter(Collider other) 
	{
		if (other.gameObject.CompareTag ("HookPoint")) 
		{
			craneController.OnTriggerEnter (other);
		}
	}
	
	void OnTriggerExit(Collider other)
	{
		if (other.gameObject.CompareTag ("HookPoint")) 
		{
			craneController.OnTriggerExit (other);
		}
	}

}
