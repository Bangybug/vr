﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour 
{
	public int craneCellsCount = 1;
	public GameObject crane;

	private CraneController craneController;



	void Awake()
	{
	}

	// Use this for initialization
	void Start () 
	{
		// XXX why null?
		//craneController = (CraneController) GameObject.Find("Crane").GetComponent(typeof(CraneController) ); 
		craneController = (CraneController)crane.GetComponent(typeof(CraneController) ); 
		craneController.setCells (craneCellsCount);
		craneController.setArrowCells (4);
	}
	
}
