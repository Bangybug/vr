using UnityEngine;
using System.Collections;

// Require a Rigidbody and LineRenderer object for easier assembly
[RequireComponent (typeof (Rigidbody))]
[RequireComponent (typeof (LineRenderer))]

/**
 * RopeScript can be added to scene. The source and target should be set. The rope is then created with a call to BuildRope(). 
 * @author Dmitry Pryadkin
 */ 
public class RopeScript : MonoBehaviour 
{
	/**
	 * The maximum length of the rope. Should call BuildRope() after changing this.
	 */
	public float ropeLength = 1.0f;

	/**
	 * Adjusted length of the rope (may be less than ropeLength)
	 */
	private float currentRopeLength;

	/**
	 * The source object to attach this rope. It should have RigidBody set. 
	 */
	public Transform source;

	/**
	 * The target object this rope is attached to. It may have RigidBody.
	 */
	public Transform target;

	/**
	 * The count of segments is calculated as: ropeLength * resolution 
	 */
	public float resolution = 0.5F;

	/**
	 * The drag force between the joints. The more drag - the heaviest the rope will behave.
	 */ 
	public float ropeDrag = 0.1F;

	/**
	 * The mass of each joint.
	 */
	public float ropeMass = 0.1F;

	/**
	 * Each rope segment has a small spherical collider with a radius.
	 */
	public float ropeColRadius = 0.5F;

	private Vector3[] segmentPos;  // XXX public for debug
	private GameObject[] joints;
	private LineRenderer line;
	private int segments = 0; 
    private float segLength = 1.0f; // XXX public for debug
    private int segmentsHidden = 0; // XXX public for debug
    private float remainingSegLength; // XXX variable for debug

	private Vector3 jointSeparation;
	private bool rope = false;
	
	//Joint Settings
	public Vector3 swingAxis = new Vector3(1,1,1);				 //  Sets which axis the character joint will swing on (1 axis is best for 2D, 2-3 axis is best for 3D (Default= 3 axis))
	public float lowTwistLimit = -100.0F;					//  The lower limit around the primary axis of the character joint. 
	public float highTwistLimit = 100.0F;					//  The upper limit around the primary axis of the character joint.
	public float swing1Limit  = 20.0F;					//	The limit around the primary axis of the character joint starting at the initialization point.

	public Transform currentSeg;

	void Start()
	{
	}
	
	void Update()
	{
	}

	void LateUpdate()
	{
		if(rope) 
		{
			for(int i=0;i<segments;i++) 
			{
				if(i == 0) 
				{
					line.SetPosition(i,source.position);
				} 
				else if(i == segments-1) 
				{
					line.SetPosition(i,target.position);	
				} 
				else 
				{
					line.SetPosition(i,joints[i].transform.position);
				}
			}
			line.enabled = true;
		} 
		else 
		{
			line.enabled = false;	
		}
	}
	
	
	
	public void BuildRope()
	{
		if (rope)
			this.DestroyRope ();

		line = gameObject.GetComponent<LineRenderer>();
		
		// Find the amount of segments based on the distance and resolution
		// Example: [resolution of 1.0 = 1 joint per unit of distance]
		segments = (int)(ropeLength*resolution);
		currentRopeLength = ropeLength;

		line.SetVertexCount(segments);
		segmentPos = new Vector3[segments];
		joints = new GameObject[segments];
		segmentPos[0] = source.position;
		segmentPos[segments-1] = target.position;
		
		// Find the distance between each segment
		var segs = segments-1;
		jointSeparation = ((target.position - source.position)/segs);
		segLength = jointSeparation.magnitude;
		segmentsHidden = 0;

		for(int s=1;s < segments;s++)
		{
			// Find the each segments position using the slope from above
			Vector3 vector = (jointSeparation*s) + source.position;	
			segmentPos[s] = vector;
			
			//Add Physics to the segments
			AddJointPhysics(s);
		}
		
		// Attach the joints to the target object and parent it to this object	
		CharacterJoint end = target.gameObject.AddComponent<CharacterJoint>();
		end.connectedBody = joints[joints.Length-1].transform.GetComponent<Rigidbody>();
		end.swingAxis = swingAxis;
		SoftJointLimit limit_setter = end.lowTwistLimit;
		limit_setter.limit = lowTwistLimit;
		end.lowTwistLimit = limit_setter;
		limit_setter = end.highTwistLimit;
		limit_setter.limit = highTwistLimit;
		end.highTwistLimit = limit_setter;
		limit_setter = end.swing1Limit;
		limit_setter.limit = swing1Limit;
		end.swing1Limit = limit_setter;
		target.SetParent (transform, true); // if false, the target is falling from source position
		
		// Rope = true, The rope now exists in the scene!
		rope = true;
	}
	
	void AddJointPhysics(int n)
	{
		joints[n] = new GameObject("Joint_" + n);
		joints [n].transform.SetParent (source, false);
		//joints[n].transform.parent = source;

		Rigidbody rigid = joints[n].AddComponent<Rigidbody>();

		SphereCollider col = joints[n].AddComponent<SphereCollider>();
		CharacterJoint ph = joints[n].AddComponent<CharacterJoint>();
		ph.swingAxis = swingAxis;
		SoftJointLimit limit_setter = ph.lowTwistLimit;
		limit_setter.limit = lowTwistLimit;
		ph.lowTwistLimit = limit_setter;
		limit_setter = ph.highTwistLimit;
		limit_setter.limit = highTwistLimit;
		ph.highTwistLimit = limit_setter;
		limit_setter = ph.swing1Limit;
		limit_setter.limit = swing1Limit;
		ph.swing1Limit = limit_setter;

		// /*
//	    SoftJointLimitSpring springLimit = ph.swingLimitSpring;
//		springLimit.spring = 0.1f;
//		springLimit.damper = 13.0f;
//		ph.swingLimitSpring = springLimit;
		//*/

		joints[n].transform.position = segmentPos[n];
		
		rigid.drag = ropeDrag;
		rigid.mass = ropeMass;
		col.radius = ropeColRadius;
		
		if(n==1)
		{		
			ph.connectedBody = source.GetComponent<Rigidbody>();
		} 
		else
		{
			ph.connectedBody = joints[n-1].GetComponent<Rigidbody>();	
		}
		
	}
	
	void DestroyRope()
	{
		// Stop Rendering Rope then Destroy all of its components
		rope = false;
		for(int dj=0;dj<joints.Length-1;dj++)
		{
			Destroy(joints[dj]);	
		}
		
		segmentPos = new Vector3[0];
		joints = new GameObject[0];
		segments = 0;
	}


	public void adjustRope(float delta)
	{
		Rigidbody rb;
		float newRopeLength = Mathf.Clamp (currentRopeLength + delta, 0.0f, ropeLength);
		if (newRopeLength != currentRopeLength) 
		{ 
			currentRopeLength = newRopeLength;
			float hiddenLength = ropeLength - currentRopeLength;
			int hideSegments = Mathf.FloorToInt( hiddenLength / segLength );
			float remainingSegLength = segLength - (hiddenLength - segLength*hideSegments);

			if (remainingSegLength + delta <= 0.0f)
			{
				remainingSegLength = 0.0f;
				++hideSegments;
			}

			for (int i=0; i<hideSegments; ++i)
			{
				line.SetPosition(i,source.position);
				rb = joints[i+1].GetComponent<Rigidbody>();
				rb.isKinematic = true;
				rb.position = source.position;
			}

			for (int i=hideSegments; i<segmentsHidden; ++i)
			{
				rb = joints[i+1].GetComponent<Rigidbody>();
				rb.position =  segmentPos[i+1];	
				rb.isKinematic = false;
			}

			rb = joints[hideSegments+1].GetComponent<Rigidbody>();
			if (remainingSegLength+delta >= segLength)
			{
				rb.isKinematic = false;
				rb.transform.position = source.position;
			}
			else
			{
				rb.isKinematic = true;

				rb.transform.position = new Vector3(rb.transform.position.x,
				                                 	source.position.y - remainingSegLength,
				                                    rb.transform.position.z );
				rb.transform.hasChanged = true;

				//Debug.Log("Tr: "+rb.transform.position.ToString());
			}

			this.remainingSegLength = remainingSegLength;

			segmentsHidden = hideSegments;

		}
	}
}