using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Pool : Object
{
	public GameObject pooledObject;
	public bool willGrow = true;
	public int pooledAmount = 0;
	
	public List<GameObject> pooledObjects;

	public Pool()
	{
		// need for component-driven usage
	}

	public Pool(GameObject gameObject, int pooledAmount)
	{
		// use for component-less usage
		init (gameObject, pooledAmount);
	}

	private void init( GameObject gameObject, int pooledAmount )
	{
		this.pooledAmount = pooledAmount;
		this.pooledObject = gameObject;
		pooledObjects = new List<GameObject>();
		for(int i = 0; i < pooledAmount; i++)
		{
			GameObject obj = (GameObject)Instantiate(pooledObject);
			obj.SetActive(false);
			pooledObjects.Add(obj);
		}
	}

	void Start()
	{
		init (pooledObject, pooledAmount);
	}

	public void deactivateAll()
	{
		for (int i = 0; i< pooledObjects.Count; i++) {
			if (pooledObjects [i] != null)
				pooledObjects [i].SetActive (false);
		}
	}
	
	public GameObject GetPooledObject()
	{
		for(int i = 0; i< pooledObjects.Count; i++)
		{
			if(pooledObjects[i] == null)
			{
				GameObject obj = (GameObject)Instantiate(pooledObject);
				obj.SetActive(false);
				pooledObjects[i] = obj;
				return pooledObjects[i];
			}
			if(!pooledObjects[i].activeInHierarchy)
			{
				return pooledObjects[i];
			}    
		}
		
		if (willGrow)
		{
			GameObject obj = (GameObject)Instantiate(pooledObject);
			pooledObjects.Add(obj);
			return obj;
		}
		
		return null;
	}
	
}