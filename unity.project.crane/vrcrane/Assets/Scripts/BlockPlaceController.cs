﻿using UnityEngine;
using System.Collections;

public class BlockPlaceController : MonoBehaviour 
{
    /// <summary>
    /// See BlockInfo.blockType
    /// </summary>
    public string acceptsBlockType = "";

    public Color acceptColor;
    private Color defaultColor;


    /// <summary>
    /// To prevent more than two BlockPlaces accepting the same object. This is the Block object.
    /// </summary>
    public static Transform currentAccepted;

    /// <summary>
    /// Only if it equals to currentAccepted this controller shall handle it in Update 
    /// </summary>
    public Transform ownAccepted;


	void Start () 
	{
        defaultColor = GetComponent<MeshRenderer>().material.color;

        LineRenderer line = gameObject.GetComponent<LineRenderer>();

        Bounds bounds = GetComponent<MeshRenderer>().bounds;

        Vector3 v3Center = bounds.center;
        Vector3 v3Extents = bounds.extents;
        
        Vector3 v3FrontTopLeft = new Vector3(v3Center.x - v3Extents.x, v3Center.y + v3Extents.y, v3Center.z - v3Extents.z);  
        Vector3 v3FrontTopRight = new Vector3(v3Center.x + v3Extents.x, v3Center.y + v3Extents.y, v3Center.z - v3Extents.z);  
        Vector3 v3FrontBottomLeft = new Vector3(v3Center.x - v3Extents.x, v3Center.y - v3Extents.y, v3Center.z - v3Extents.z);  
        Vector3 v3FrontBottomRight = new Vector3(v3Center.x + v3Extents.x, v3Center.y - v3Extents.y, v3Center.z - v3Extents.z);
        Vector3 v3BackTopLeft = new Vector3(v3Center.x - v3Extents.x, v3Center.y + v3Extents.y, v3Center.z + v3Extents.z); 
        Vector3 v3BackTopRight = new Vector3(v3Center.x + v3Extents.x, v3Center.y + v3Extents.y, v3Center.z + v3Extents.z);  
        Vector3 v3BackBottomLeft = new Vector3(v3Center.x - v3Extents.x, v3Center.y - v3Extents.y, v3Center.z + v3Extents.z);  
        Vector3 v3BackBottomRight = new Vector3(v3Center.x + v3Extents.x, v3Center.y - v3Extents.y, v3Center.z + v3Extents.z);  

        line.SetVertexCount(15);

        line.SetPosition(0, v3FrontTopLeft);
        line.SetPosition(1, v3FrontTopRight);   
        line.SetPosition(2, v3FrontBottomRight);
        line.SetPosition(3, v3FrontBottomLeft);    
        line.SetPosition(4, v3FrontTopLeft);

        line.SetPosition(5, v3BackTopLeft);
        line.SetPosition(6, v3BackTopRight);
        line.SetPosition(7, v3FrontTopRight);
        line.SetPosition(8, v3FrontBottomRight);

        line.SetPosition(9, v3BackBottomRight);
        line.SetPosition(10, v3BackTopRight);

        line.SetPosition(11, v3BackTopLeft);           
        line.SetPosition(12, v3BackBottomLeft);
        line.SetPosition(13, v3FrontBottomLeft);
        line.SetPosition(13, v3BackBottomLeft);
        line.SetPosition(14, v3BackBottomRight);

	}

    /// <summary>
    /// Even if collision is reported, the alignment may still be not good (for example, a wall lies down but still collides the acceptor)
    /// </summary>
    /// <returns><c>true</c>, if  alignment is good, <c>false</c> otherwise.</returns>
    /// <param name="block">Block.</param>
    private bool checkProperAlignment(BoxCollider bc)
    {
        Bounds thisBounds = GetComponent<BoxCollider>().bounds;
        Vector3 minOther = bc.bounds.min;
        Vector3 minThis = thisBounds.min;

        float xCovered = thisBounds.size.x - Mathf.Abs(minOther.x - minThis.x);
        float yCovered = thisBounds.size.y - Mathf.Abs(minOther.y - minThis.y);
        float zCovered = thisBounds.size.z - Mathf.Abs(minOther.z - minThis.z);

        float coverageRatio = (xCovered * yCovered * zCovered) / thisBounds.size.magnitude;
        return coverageRatio > 0.7; // 70% coverage we think is good
    }


    void OnTriggerEnter(Collider other) 
    {
        if (null == currentAccepted)
        {
            if (other.gameObject.CompareTag("BlockFoot"))
            {
                // the event comes but we have re-composed the block object - we should ignore it here
                if (other.transform.parent == null) return;

                BlockInfo blockInfo = other.gameObject.transform.parent.GetComponent<BlockInfo>();
                if (blockInfo.blockType.Equals(acceptsBlockType))
                {
                    GetComponent<MeshRenderer>().material.color = acceptColor;
                    ownAccepted = currentAccepted = other.gameObject.transform.parent;
                }
            }
        }
    }
    
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag ("BlockFoot")) 
        {
            if (currentAccepted == other.gameObject.transform.parent && ownAccepted == currentAccepted)
            {
                GetComponent<MeshRenderer>().material.color = defaultColor;
                ownAccepted = currentAccepted = null;
            }
        }
    }


    void Update()
    {
        // we have accepted object
        if (null != currentAccepted && ownAccepted == currentAccepted)
        {
            // it is not hooked
            if (CraneController.hookedBlock != currentAccepted)
            {
                // TODO check coverage - it's important

                // and it nearly has stopped falling
                Rigidbody acceptedRigidBody = currentAccepted.gameObject.GetComponent<Rigidbody>();
                if (acceptedRigidBody.velocity.sqrMagnitude <= 0.01f)
                {
                    // the block is going to be re-composed, its Plate will remain, all else should pass away
                    Transform plate = currentAccepted.FindChild("Plate");
                    plate.SetParent( transform.parent );
                    plate.localScale = transform.localScale;
                    plate.localRotation = transform.localRotation;
                    plate.localPosition = transform.localPosition;

                    Destroy(currentAccepted.gameObject);
                    ownAccepted = currentAccepted = null;

                    // no longer need this acceptor
                    gameObject.SetActive(false);

                }
            }
        }       
    }


}
