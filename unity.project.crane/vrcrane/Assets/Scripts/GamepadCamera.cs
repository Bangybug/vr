﻿using UnityEngine;
using System.Collections;

public class GamepadCamera : MonoBehaviour 
{
    public ECameraMode cameraMode = ECameraMode.Free;

    public Transform cameraWalker;
    private Rigidbody walkerBody;

    private float rotationSpeed = 16f;
    private float impulseRotationSpeed = 32.0f;
    private float movementSpeed = 3f;
    private float rotationX = -43, rotationY = -33;

    private bool trackVR;

    public enum ECameraMode
    {
        Free,
        PlaneWalk
    }


	// Use this for initialization
	void Awake () 
    {
        transform.localRotation = Quaternion.AngleAxis(rotationX, Vector3.up);
        transform.localRotation *= Quaternion.AngleAxis(rotationY, Vector3.left);
        walkerBody = cameraWalker.GetComponent<Rigidbody>();
        trackVR = transform.GetComponent<CardboardHead>().enabled;
	}
	

	// Update is called once per frame
	void LateUpdate () 
    {
        // camera with collision detection
        if (cameraMode == ECameraMode.PlaneWalk)
        {
            transform.position = cameraWalker.position;
            walkerBody.rotation = transform.rotation;
            walkerBody.AddForce( getMovementDirection(), ForceMode.VelocityChange );

            //Vector3 pos = Cardboard.SDK.HeadPosition;
            //transform.position = transform.position + transform.rotation * pos;

            updateRotation(impulseRotationSpeed);
        } 
        // free camera
        else
        {
            transform.Translate(getMovementDirection() * movementSpeed * Time.deltaTime);
            updateRotation(rotationSpeed);
        }

        if (Input.GetButtonDown("WalkFlySwitch"))
        {
            cameraMode = cameraMode == ECameraMode.PlaneWalk ? ECameraMode.Free : ECameraMode.PlaneWalk;
            if (cameraMode == ECameraMode.PlaneWalk)
            {
                walkerBody.position = transform.position;
            }
        }
	}


    private void updateRotation(float rotationSpeed)
    {
        if (trackVR)
            return;

        float dx = Input.GetAxis("x");
        float dy = -Input.GetAxis("y");

        if (dx != 0.0f && dy != 0.0f)
        {
            rotationX += dx * rotationSpeed * Time.deltaTime;
            rotationY -= dy * rotationSpeed * Time.deltaTime;
            rotationY = Mathf.Clamp(rotationY, -90, 90);
            transform.localRotation = Quaternion.AngleAxis(rotationX, Vector3.up);
            transform.localRotation *= Quaternion.AngleAxis(rotationY, Vector3.left);
        }
    }


    private Vector3 getMovementDirection() 
    {
        Vector3 p_Velocity = new Vector3();

        if (Input.GetButton("MoveForward"))
        {
            p_Velocity += transform.forward;
        }
        if (Input.GetButton("MoveBackward"))
        {
            p_Velocity -= transform.forward;
        }
        if (Input.GetButton("MoveRight"))
        {
            p_Velocity += transform.right;
        }
        if (Input.GetButton("MoveLeft"))
        {
            p_Velocity -= transform.right;
        }

        return p_Velocity;
    }
}
