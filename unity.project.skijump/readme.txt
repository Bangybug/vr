Compiled with Unity 5.3

Unpack cardboard sdk into  ski / Assets folder.

If you see empty space after opening this in Unity, then find SlopeTerrain scene in the Assets folder and double click it.



How it works:

You will have to stay straight, center your look at the "Go" button so it highlights, wait a few seconds to start your run.

Your goal is to control your speed before reaching the ramp. Too much speed and you fly behind landing zone (bad). 

Not enough speed and you land in the transit zone (less bad, but also bad).

The speed just needs to be enough to jump from the ramp and land in the landing zone.

To reach the speed, you will need to be straight (your virtual feet needs to be looking at the ramp). Tilting the phone will cause a turn with a natural loss of speed.