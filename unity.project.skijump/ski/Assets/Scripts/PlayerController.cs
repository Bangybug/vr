﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerController : MonoBehaviour {

    public Cardboard cardboardRef;
    public Button kickButtonRef;
    public Canvas uiCanvasRef;
    public Camera cameraRef;

    public static Vector3 velocity = Vector3.zero;
    private Rigidbody rb;
    private bool kicked = false;

    private Vector3 initialPlayerPos;
    private Quaternion initialPlayerRot;

    public PhysicMaterial skiPhysicsMaterialRef;

    public float debugFric, debugRotDelta;


    // Use this for initialization
    void Start () {
        rb = this.GetComponent<Rigidbody>();
        initialPlayerPos = new Vector3( this.transform.position.x, this.transform.position.y, this.transform.position.z );
        initialPlayerRot = new Quaternion( rb.rotation.x, rb.rotation.y, rb.rotation.z, rb.rotation.w );
    }

    public void instantTorque(float torqueDelta)
    {
        rb.AddRelativeTorque(Vector3.up * torqueDelta, ForceMode.Acceleration);
        //rb.AddTorque(Vector3.up * torqueDelta, ForceMode.Impulse);
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.W))
            onKick();

        velocity = rb.velocity;

        float torqueDelta = 0.0f;
        if (Input.GetKey(KeyCode.A))
            torqueDelta = -0.05f;
        else if (Input.GetKey(KeyCode.D))
            torqueDelta = 0.05f;

        float angle = rb.rotation.eulerAngles.y;
        if (angle > 180.0f && angle <= 360.0f)
        {
            angle += 90;
            debugRotDelta = Mathf.Sin(angle * Mathf.PI / 360.0f);
        }
        else
        {
            angle += 90;
            debugRotDelta = Mathf.Cos(angle * Mathf.PI / 360.0f);
        }
        debugRotDelta = Mathf.Abs(debugRotDelta);

        const float frictionMax = 0.9f;
        const float frictionMin = 0.05f;
        
        float friction = frictionMin + (frictionMax - frictionMin) * debugRotDelta;
        skiPhysicsMaterialRef.dynamicFriction = friction;

        debugFric = friction;

        if (torqueDelta != 0.0f)
            instantTorque(torqueDelta);

        if (rb.position.y < 0 || (kicked && rb.velocity.magnitude < 0.01f))
            startAgain();

    }


    public void startAgain()
    {
        uiCanvasRef.enabled = true;
        kickButtonRef.enabled = true;
        rb.isKinematic = true;
        rb.velocity = Vector3.zero;
        rb.position = new Vector3(initialPlayerPos.x, initialPlayerPos.y, initialPlayerPos.z);
        rb.rotation = new Quaternion(initialPlayerRot.x, initialPlayerRot.y, initialPlayerRot.z, initialPlayerRot.w);
        rb.isKinematic = false;
        kicked = false;
    }

    public void onKick()
    {
        uiCanvasRef.enabled = false;
        kickButtonRef.enabled = false;
        if (rb.velocity.magnitude < 0.1f)
        {
            rb.velocity = new Vector3(-1.1f, -0.0f, 0.0f);
            kicked = true;
        }
    }

    void OnCollisionExit(Collision collisionInfo)
    {
       
    }
}
