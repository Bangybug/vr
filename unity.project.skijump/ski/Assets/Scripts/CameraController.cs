﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

    float camSens = .25f;  // Camera sensitivity by mouse input.
    private Vector3 lastMouse = new Vector3(Screen.width / 2, Screen.height / 2, 0); // Kind of in the middle of the screen, rather than at the top (play).

    private bool isOn = true;
    private bool trackVR = false;

    public PlayerController playerControllerRef;

    public float debugTilt;
    private float oldTilt;
    public float debugTorque;
    public float debugDiff;

    void Start()
    {
        trackVR = transform.GetComponent<CardboardHead>().enabled;
    }


    void LateUpdate()
    {
        if (!isOn)
            return;

        if (trackVR)
        {
            // use head rotation to drive torques
            //Debug.Log(transform.position.ToString());
            debugTilt = transform.eulerAngles.z;

            const float neutralThreshold = 5;
            const float tilt2Torque = 5f;

            if ( (debugTilt < 85 && debugTilt < neutralThreshold) || 
                 (debugTilt > 270 && debugTilt > (360-neutralThreshold)))
            {
                // neutral
                oldTilt = 0.0f;
                debugTorque = -99;
            }
            else
            {
                debugTorque = -1;
                float diff = debugTilt - oldTilt;

                debugDiff = diff;

                if ((debugTilt < 85 && diff > 0.0f) ||
                    (debugTilt > 270 && diff < 0.0f))
                {
                    diff *= -1.0f;
                    float torque = diff * tilt2Torque;
                    playerControllerRef.instantTorque(torque);
                    debugTorque = torque;
                }                
                oldTilt = debugTilt;

            }
            
        }
        else
        {
            // Mouse input.
            lastMouse = Input.mousePosition - lastMouse;
            lastMouse = new Vector3(-lastMouse.y * camSens, lastMouse.x * camSens, 0);
            lastMouse = new Vector3(transform.eulerAngles.x + lastMouse.x, transform.eulerAngles.y + lastMouse.y, 0);
            transform.eulerAngles = lastMouse;
            lastMouse = Input.mousePosition;
        }
    }

  
}
