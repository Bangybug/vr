﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class TerrainFromPng : MonoBehaviour {

    [MenuItem("Tools/dpryadkin_TerrainFromTexture")]
    private static void TerrainFromTexture()
    {
        Texture2D heightmap = Selection.activeObject as Texture2D;
        if (heightmap == null)
        {
            EditorUtility.DisplayDialog("No texture selected", "Please select a texture.", "Cancel");
            return;
        }

        Undo.RegisterUndo(Terrain.activeTerrain.terrainData, "Heightmap From Texture");

        var terrain = Terrain.activeTerrain.terrainData;
        var w = heightmap.width;
        var h = heightmap.height;
        var w2 = terrain.heightmapWidth;
        var heightmapData = terrain.GetHeights(0, 0, w2, w2);
        var mapColors = heightmap.GetPixels();
        var map = new Color[w2 * w2];

        Debug.Log("Terrain dimensions: " + w.ToString() + ", " + h.ToString() + ", " + w2.ToString());

        if ( (w2-1) != w || h != w)
        {
            // Resize using nearest-neighbor scaling if texture has no filtering
            if (heightmap.filterMode == FilterMode.Point)
            {
                float dx = (float)w / w2;
                float dy = (float)h / w2;
                for (int y = 0; y < w2-1; y++)
                {
                    if (y % 20 == 0)
                    {
                        EditorUtility.DisplayProgressBar("Resize", "Calculating texture", Mathf.InverseLerp(0.0f, w2, y));
                    }
                    var thisY = Mathf.FloorToInt((dy * y) * w);
                    var yw = y * w2;
                    for (int x = 0; x < w2-1; x++)
                    {
                        map[yw + x] = mapColors[Mathf.FloorToInt(thisY + dx * x)];
                    }
                }
            }
            // Otherwise resize using bilinear filtering
            else {
                float ratioX = 1.0f / (w2 / (w - 1));
                float ratioY = 1.0f / (w2 / (h - 1));
                for (int y = 0; y < w2; y++)
                {
                    if (y % 20 == 0)
                    {
                        EditorUtility.DisplayProgressBar("Resize", "Calculating texture", Mathf.InverseLerp(0.0f, w2, y));
                    }
                    var yy = Mathf.Floor(y * ratioY);
                    var y1 = yy * w;
                    var y2 = (yy + 1) * w;
                    float yw = y * w2;
                    for (int x = 0; x < w2; x++)
                    {
                        var xx = Mathf.Floor(x * ratioX);

                        var bl = mapColors[Mathf.FloorToInt(y1 + xx)];
                        var br = mapColors[Mathf.FloorToInt(y1 + xx + 1)];
                        var tl = mapColors[Mathf.FloorToInt(y2 + xx)];
                        var tr = mapColors[Mathf.FloorToInt(y2 + xx + 1)];

                        var xLerp = x * ratioX - xx;
                        map[Mathf.FloorToInt(yw + x)] = Color.Lerp(Color.Lerp(bl, br, xLerp), Color.Lerp(tl, tr, xLerp), y * ratioY - yy);
                    }
                }
            }
            EditorUtility.ClearProgressBar();
        }
        else {
            // Use original if no resize is needed
            map = mapColors;
        }

        // Assign texture data to heightmap
        for (int y = 0; y < w2-1; y++)
        {
            for (int x = 0; x < w2-1; x++)
            {
                heightmapData[y, x] = map[y * (w2-1) + x].grayscale;
            }
        }
        terrain.SetHeights(0, 0, heightmapData);

    }

}
